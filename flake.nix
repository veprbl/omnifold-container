{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/release-23.05";

  outputs = { self, nixpkgs }: {

    packages.x86_64-linux.default =
      with import nixpkgs {
        system = "x86_64-linux";
        config.allowUnfreePredicate = _: true;
      };
      let
        nvidia-pkgs =
          import (nixpkgs + /pkgs/os-specific/linux/nvidia-x11/generic.nix) {
              version = "550.54.14";
              sha256_64bit = "sha256-jEl/8c/HwxD7h1FJvDD6pP0m0iN7LLps0uiweAFXz+M=";
              openSha256 = "sha256-F+9MWtpIQTF18F2CftCJxQ6WwpA8BVmRGEq3FhHLuYw=";
              settingsSha256 = "sha256-m2rNASJp0i0Ez2OuqL+JpgEF0Yd8sYVCyrOoo/ln2a4=";
              persistencedSha256 = "sha256-XaPN8jVTjdag9frLPgBtqvO/goB5zxeGzaTU0CdL6C4=";
          };
        nvidia-x11-lcc = (callPackage nvidia-pkgs {
            libsOnly = true;
            pkgs = pkgs // {
              libdrm = null;
              xorg.libXext = null;
              xorg.libX11 = null;
              xorg.libXv = null;
              xorg.libXrandr = null;
              xorg.libxcb = null;
              wayland = null;
              mesa = null;
              libGL = null;
            };
            disable32Bit = true;
        }).overrideAttrs (prev: {
          nativeBuildInputs = prev.nativeBuildInputs ++ [ zstd ]; # for unpackPhase
        });

        packages = [
          bashInteractive
          cacert
          coreutils
          curl
          emacs
          gawk
          gnugrep
          gnused
          jq
          less
          perl
          procps
          rsync
          silver-searcher
          vim
          which
          wget
          zsh          

          python3
          (python3Packages.energyflow.overrideAttrs (prev: {
            postPatch = (prev.postPatch or "") + ''
              substituteInPlace energyflow/utils/data_utils.py \
                --replace "np.int" "int"
            '';
          }))
          python3Packages.keras
          python3Packages.matplotlib
          python3Packages.numpy
          python3Packages.pypdf2
          python3Packages.scipy
          python3Packages.scikitlearn
          python3Packages.tensorflowWithCuda
          nvidia-x11-lcc
        ];

        container_env = pkgs.runCommandNoCC "container-env" {
          buildInputs = packages;
        } ''
          mkdir -p "$out/.singularity.d/env"
          declare -p | grep -vE "^declare -[ai-]" | grep -vE "^declare -. (PWD|OLDPWD|HOME|TMP|TEMP|BASHOPTS|SHELLOPTS)" > "$out/.singularity.d/env/99-omnifold_container.sh"
          echo 'export LD_LIBRARY_PATH=${lib.makeLibraryPath [ nvidia-x11-lcc ]}:$LD_LIBRARY_PATH' >> "$out/.singularity.d/env/99-omnifold-container.sh"
        '';
      in
      dockerTools.buildLayeredImage {
        name = "omnifold";
        contents = packages ++ [ container_env ];
      };

  };
}
